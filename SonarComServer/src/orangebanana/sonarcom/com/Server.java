package orangebanana.sonarcom.com;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import javax.sound.sampled.AudioInputStream;

class Server {

    AudioInputStream audioInputStream;
    static boolean status = true;
    static int port = 50005;
    static String DEST_IP = "172.27.0.119";

    public static void main(String args[]) throws Exception {

        DatagramSocket serverSocket = new DatagramSocket(50005);


        byte[] receiveData = new byte[1280];
        // ( 1280 for 16 000Hz and 3584 for 44 100Hz (use AudioRecord.getMinBufferSize(sampleRate, channelConfig, audioFormat) to get the correct size)


        while (status == true) {
            DatagramPacket receivePacket = new DatagramPacket(receiveData,
                    receiveData.length);
            serverSocket.receive(receivePacket);
            byte soundbytes[] = receivePacket.getData();
            System.out.println("Data received!");


            DatagramPacket packet;
            final InetAddress destination = InetAddress.getByName(DEST_IP);
            packet = new DatagramPacket (soundbytes, soundbytes.length, destination, port);
//            DatagramSocket socket = new DatagramSocket();
            serverSocket.send(packet);
            System.out.println("Data sent!");
//            socket.close();
        }
    }
}