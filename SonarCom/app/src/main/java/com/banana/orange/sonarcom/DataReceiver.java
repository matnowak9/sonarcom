package com.banana.orange.sonarcom;

import android.util.Log;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.nio.ShortBuffer;

/**
 * Created by arccha on 3/18/16.
 */
public class DataReceiver implements Runnable {

    private static final int PORT = 9876;

    DatagramSocket clientsocket;

    DataReceiver () {
        try {
            clientsocket = new DatagramSocket(PORT);
        } catch (SocketException e) {
            Log.e("Socket", "Creating datagram socket failed.");
        }
    }

    public byte[] recvData() {
        byte[] recvData = new byte[1024];
        try {
            DatagramPacket recvPacket = new DatagramPacket(recvData, recvData.length);
            clientsocket.receive(recvPacket);
            String rec_str = new String(recvPacket.getData());
            Log.d("Received ", rec_str);
        } catch (Exception e) {
            Log.e("UDP", "S: Error", e);
        }
        return recvData;
    }

    @Override
    public void run() {
        while (true) {
            byte[] data = recvData();
            ShortBuffer sb = ShortBuffer.allocate(data.length/2);
            for (int i=0; i*2 < data.length; i+=2)
                sb.put((short) ((data[i + 1] << 8) + (data[i] & 0xFF)));
        }
    }
}
