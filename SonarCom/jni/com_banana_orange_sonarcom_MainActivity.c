#include "com_banana_orange_sonarcom_MainActivity.h"


#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include <AL/al.h>
#include <AL/alc.h>


ALCdevice* device;
ALCcontext* context;

JNIEXPORT jboolean JNICALL Java_com_banana_orange_sonarcom_MainActivity_init
(JNIEnv *env, jobject obj){

	 // Global Variables
	 device = 0;
	 context = 0;
	 const ALint context_attribs[] = { 
// 	 	// ALC_FREQUENCY, 44100,
	 	 0 };

	 // Initialization
	 device = alcOpenDevice(0);
	 jboolean l = 0;
	 l =  alcDeviceEnableHrtfMOB( device, AL_TRUE );
	 context = alcCreateContext(device, context_attribs);
	 alcMakeContextCurrent(context);


	 return l;
  }
JNIEXPORT jint JNICALL Java_com_banana_orange_sonarcom_MainActivity_play
  (JNIEnv *env, jobject obj, jshortArray array, jint size, jint x, jint y) {


	//  // Create source from buffer and play it
	 ALuint source = 0;
	 alGenSources(1, &source );

	 ALuint buffer = 0;

	 alGenBuffers(1,&buffer);
		
	// printf("test\n");
	// short *tab = (short *)(array);
	// short tab[] = {12,123};
	
	jsize len = (*env)->GetArrayLength(env, array);
    jshort *body = (*env)->GetShortArrayElements(env, array, 0);

	alBufferData(buffer,AL_FORMAT_MONO16,body,len,8000);

	alSourcei(source, AL_BUFFER, buffer);
	float SourcePos[] = { x, y, 0};
	// alSourcei (source, AL_LOOPING,  1);

	alSourcefv(source, AL_POSITION, SourcePos);

	 // Play source
	 alSourcePlay(source);

	 int sourceState = AL_PLAYING;
	 do {
	 	alGetSourcei(source, AL_SOURCE_STATE, &sourceState);
	 } while(sourceState == AL_PLAYING);

	//  // Release source
	 alDeleteSources(1, &source);


	 alDeleteBuffers(1, &buffer);




	 return alGetError();
}

JNIEXPORT void JNICALL Java_com_banana_orange_sonarcom_MainActivity_deInit
  (JNIEnv *env, jobject obj){
  	 // Cleaning up
 alcMakeContextCurrent(0);
 alcDestroyContext(context);
 alcCloseDevice(device);

}

