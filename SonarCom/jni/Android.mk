TARGET_PLATFORM := android-23
ROOT_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_PATH       := $(ROOT_PATH)
LOCAL_C_INCLUDES := $(LOCAL_PATH) $(LOCAL_PATH)/../OpenAL-MOB/include $(LOCAL_PATH)/../OpenAL-MOB/OpenAL32/Include $(LOCAL_PATH)/../OpenAL-MOB/mob/Include
LOCAL_MODULE    := openal
LOCAL_SRC_FILES := ../OpenAL-MOB/Alc/ALc.c ../OpenAL-MOB/Alc/alcConfig.c ../OpenAL-MOB/Alc/alcDedicated.c ../OpenAL-MOB/Alc/alcEcho.c ../OpenAL-MOB/Alc/alcModulator.c ../OpenAL-MOB/Alc/alcReverb.c ../OpenAL-MOB/Alc/alcRing.c ../OpenAL-MOB/Alc/alcThread.c ../OpenAL-MOB/Alc/ALu.c ../OpenAL-MOB/Alc/backends/loopback.c ../OpenAL-MOB/Alc/backends/null.c ../OpenAL-MOB/Alc/backends/opensl.c ../OpenAL-MOB/Alc/backends/wave.c ../OpenAL-MOB/Alc/bs2b.c ../OpenAL-MOB/Alc/helpers.c ../OpenAL-MOB/Alc/hrtf.c ../OpenAL-MOB/Alc/mixer.c ../OpenAL-MOB/Alc/mixer_c.c ../OpenAL-MOB/Alc/mixer_neon.c ../OpenAL-MOB/Alc/mixer_sse.c ../OpenAL-MOB/Alc/panning.c ../OpenAL-MOB/mob/alConfigMob.c ../OpenAL-MOB/OpenAL32/alAuxEffectSlot.c ../OpenAL-MOB/OpenAL32/alBuffer.c ../OpenAL-MOB/OpenAL32/alEffect.c ../OpenAL-MOB/OpenAL32/alError.c ../OpenAL-MOB/OpenAL32/alExtension.c ../OpenAL-MOB/OpenAL32/alFilter.c ../OpenAL-MOB/OpenAL32/alListener.c ../OpenAL-MOB/OpenAL32/alSource.c ../OpenAL-MOB/OpenAL32/alState.c ../OpenAL-MOB/OpenAL32/alThunk.c

# set the platform flags
ifeq ($(TARGET_ARCH),x86)
	LOCAL_CFLAGS += -D HAVE_SSE
else
	LOCAL_CFLAGS += -D HAVE_NEON -mfloat-abi=softfp -mfpu=neon -marm
endif


#LOCAL_SHARED_LIBRARIES += libOpenSLES
LOCAL_LDFLAGS += -lOpenSLES

include $(BUILD_SHARED_LIBRARY)

# ########################################################################################################

include $(CLEAR_VARS)

LOCAL_MODULE     := openaltest
LOCAL_ARM_MODE   := arm
LOCAL_PATH       := $(ROOT_PATH)
LOCAL_C_INCLUDES := $(LOCAL_PATH)/../OpenAL-MOB/include
LOCAL_SRC_FILES  := com_banana_orange_sonarcom_MainActivity.c     \

LOCAL_LDLIBS     := -llog -Wl,-s

LOCAL_SHARED_LIBRARIES := libopenal

include $(BUILD_SHARED_LIBRARY)

# ########################################################################################################